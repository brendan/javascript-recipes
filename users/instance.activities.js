
const GLClient = require("../libs/gl-cli").GLClient
const fs = require("fs")
const jsonexport = require('jsonexport');


async function getAllUsers({glClient, perPage, page}) {
  return glClient.getAllUsers({perPage, page})
}

async function getAllProjects({glClient, perPage, page}) {
  return glClient.getAllProjects({perPage, page})
}

async function getAllGroups({glClient, perPage, page}) {
  return glClient.getAllGroups({perPage, page})
}

async function getActivities(glClient) {
  return glClient.getActivities()
}

// === Members ===
// https://docs.gitlab.com/ee/api/members.html
// Get Members of a project
async function getProjectMembers(glClient, {projectId}) {
  return glClient.getProjectMembers({projectId})
}

// Get Members of a project
async function getGroupMembers(glClient, {groupId}) {
  return glClient.getGroupMembers({groupId})
}

// await is only valid in async function
/**
 * Get all users of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceUsers(glClient) {
  try {
    var stop = false, page = 1, users = []
    while (!stop) {
      await getAllUsers({glClient, perPage:20, page:page}).then(someUsers => {
        users = users.concat(someUsers.map(user => {
          return { 
            id:user.id,
            username:user.username, 
            web_url:user.web_url, 
            state:user.state, 
            email:user.email,
            is_admin:user.is_admin,
            external:user.external,
            can_create_group: user.can_create_group,
            can_create_project: user.can_create_project
          }
        }))
        if(someUsers.length == 0) { stop = true }
      })
      page +=1
    }
    return users
  } catch (error) {
    console.log("😡:", error)
  }
}

/**
 * Get all projects of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceProjects(glClient) {
  try {
    var stop = false, page = 1, projects = []
    while (!stop) {
      await getAllProjects({glClient, perPage:20, page:page}).then(someProject => {
        
        projects = projects.concat(someProject.map(project => {
          return project
        }))
        if(someProject.length == 0) { stop = true }
      })
      page +=1
    }
    return projects
  } catch (error) {
    console.log("😡:", error)
  }
}

/**
 * Get all groups  of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceGroups(glClient) {
  try {
    var stop = false, page = 1, groups = []
    while (!stop) {
      await getAllGroups({glClient, perPage:20, page:page}).then(someGroup => {
        groups = groups.concat(someGroup.map(group => {
          return group
        }))
        if(someGroup.length == 0) { stop = true }
      })
      page +=1
    }
    return groups
  } catch (error) {
    console.log("😡:", error)
  }
}



async function batch(gitLabClient, instanceName) {
  console.time("GitLab")
  var arrGLInstance
    , activitiesGLInstance
    , projectsGLInstance
    , groupsGLInstance
    , projectsMembersGLInstance
    , groupsMembersGLInstance

  await getInstanceUsers(gitLabClient).then(res => arrGLInstance=res)
  await getActivities(gitLabClient).then(res => activitiesGLInstance=res)
  await getInstanceProjects(gitLabClient).then(res => projectsGLInstance=res)
  await getInstanceGroups(gitLabClient).then(res => groupsGLInstance=res)


  fs.writeFileSync(`./activities/projects_${instanceName}.json`, JSON.stringify(projectsGLInstance))
  fs.writeFileSync(`./activities/groups_${instanceName}.json`, JSON.stringify(groupsGLInstance))
  jsonexport(projectsGLInstance, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./activities/projects_${instanceName}.csv`, csv)
  })

  jsonexport(groupsGLInstance, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./activities/groups_${instanceName}.csv`, csv)
  })  

  // ----- find the max role on project for each user -----
  projectsMembersGLInstance = []
  for(var i in projectsGLInstance) {
    await getProjectMembers(gitLabClient, {projectId: projectsGLInstance[i].id})
      .then(res => {
        projectsMembersGLInstance = projectsMembersGLInstance.concat(res.map(item => {
          return item
        }))
      })
  }

  fs.writeFileSync(`./activities/projects_members_${instanceName}.json`, JSON.stringify(projectsMembersGLInstance))
  jsonexport(projectsMembersGLInstance, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./activities/projects_members_${instanceName}.csv`, csv)
  })

  // ----- find the max role on Group for each user -----
  groupsMembersGLInstance = []
  for(var i in groupsGLInstance) {
    await getGroupMembers(gitLabClient, {groupId: groupsGLInstance[i].id})
      .then(res => {
        groupsMembersGLInstance = groupsMembersGLInstance.concat(res.map(item => {
          return item
        }))
      })
  }

  fs.writeFileSync(`./activities/groups_members_${instanceName}.json`, JSON.stringify(groupsMembersGLInstance))
  jsonexport(groupsMembersGLInstance, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./activities/groups_members_${instanceName}.csv`, csv)
  })

  // ----- activities affectation -----
  /*
  activitiesGLInstance.forEach(item => {
    var userInstance = arrGLInstance.find(user => user.username==item.username)
    if(userInstance) {

      var userInProject = projectsMembersGLInstance.find(user => user.id == userInstance.id)
      if(userInProject) {
        userInstance.project_access_level = userInProject.access_level
      }
      var userInGroup = groupsMembersGLInstance.find(user => user.id == userInstance.id)
      if(userInGroup) {
        userInstance.group_access_level = userInGroup.access_level
      }
      userInstance.last_activity_on = item.last_activity_on
      userInstance.last_activity_at = item.last_activity_at
    }
  });

  console.log("===================================")
  console.log(activitiesGLInstance)
  console.log("===================================")
  */

  /**
   * ===== Generate Report =====
   * 
   * - parse arrGLInstance
   * - for each user in arrGLInstance find his access_level in projectsMembersGLInstance
   * - for each user in arrGLInstance find his access_level in groupsMembersGLInstance
   * - add the information to the user
   * - generate the report (json and csv)
   */
  arrGLInstance.forEach(userInstance => {
    var userInProject = projectsMembersGLInstance.find(user => user.id == userInstance.id)
    if(userInProject) {
      userInstance.project_access_level = userInProject.access_level
    }

    var userInGroup = groupsMembersGLInstance.find(user => user.id == userInstance.id)
    if(userInGroup) {
      userInstance.group_access_level = userInGroup.access_level
    }
  })

  fs.writeFileSync(`./activities/data_${instanceName}.json`, JSON.stringify(arrGLInstance))
  jsonexport(arrGLInstance, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./activities/data_${instanceName}.csv`, csv)
  })

  console.timeEnd("GitLab")
  return {message:"done"}

}

/**
 * Instance of the GitLab client
 * you need to set the environment variables
 * 
 * ### How to run the script:
 * e.g.:
 * ```
 * TOKEN="DfaXK4wjins_ZnANyzJL" URL="http://gitlab-1.test" node instance.activities.js gitlab-1
 * ```
 */
let gitLabClient = new GLClient({
  baseUri: `${process.env.URL}/api/v4`,
  token: process.env.TOKEN
})


let gitLabInstanceName = process.argv[2]

batch(gitLabClient, gitLabInstanceName).then(result => {
  console.log(result)
})


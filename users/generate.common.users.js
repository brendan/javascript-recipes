/**
 * This script generates common users in 3 instances
 * ⚠️ don't forget to count the admin user(s)
 * 
 * TODO: add catch to promises to check if all users are created
 * the most important thing is to know if there is the same number of users in each instance
 * 
 */


const GLClient = require("../libs/gl-cli").GLClient


/**
 * Instance of the GitLab client
 * you need to set the environment variables
 * 
 * ### How to run the script:
 * e.g.:
 * ```
 * TOKEN1="DfaXK4wjins_ZnANyzJL" URL1="http://gitlab-1.test" \
 * TOKEN2="vTd4M1UykHzTUsnqU2Ju" URL2="http://gitlab-2.test" \
 * TOKEN3="34w4fMJ6iFiyy2BLVa_R" URL3="http://gitlab-3.test" \
 * node generate.common.users.js
 * ```
 */
 
let gitLabClient1 = new GLClient({
  baseUri: `${process.env.URL1}/api/v4`,
  token: process.env.TOKEN1
})
let gitLabClient2 = new GLClient({
  baseUri: `${process.env.URL2}/api/v4`,
  token: process.env.TOKEN2
})
let gitLabClient3 = new GLClient({
  baseUri: `${process.env.URL3}/api/v4`,
  token: process.env.TOKEN3
})


const faker = require('faker');

async function createUser(gitLabConnection, {username, name, email, password}) {
  return gitLabConnection.createUser({username, name, email, password})
}

async function batch() {

  // generate common users in gitLab01 and gitLab02
  try {
    for (i = 0; i <= 2; i++) { // 3 users
      let email = faker.internet.email()
      let username = name = email.substring(0, email.lastIndexOf("@"))
      let password = "ilovepandas"
      // await is only valid in async function
      await createUser(gitLabClient1, {username, name, email, password})
      await createUser(gitLabClient2, {username, name, email, password})
      await createUser(gitLabClient3, {username, name, email, password})
    }
  } catch (error) {
    console.log("😡:", error)
  }  

}

batch()
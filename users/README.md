# Users scripts

## count.instance.users

The `count.instance.users` counts the users of a GitLab instance with these informations:

- active users (including admins)
- blocked users

### run it

```
TOKEN="your-gitlab-token" URL="your-gitlab-url" node count.instance.users.js your-gitlab-instance-name
```

> For example, Here, I run the script 3 times on 3 different GitLab instances

``` 
#!/bin/sh
TOKEN="QDohC2MCsrGfF5rH3a4r" URL="http://gitlab-1.test" node count.instance.users.js gitlab-1
TOKEN="zgXDS6o5b2rBBnTHyvu6" URL="http://gitlab-2.test" node count.instance.users.js gitlab-2
TOKEN="3wxPZDxQd-GxyrB9_yBo" URL="http://gitlab-3.test" node count.instance.users.js gitlab-3
```

> And you'll get something like that
```
===== gitlab-1 =====
active users (including 1 admins): 9
blocked users: 1
>> get users of the instance
>> users ready
===== gitlab-2 =====
active users (including 1 admins): 8
blocked users: 2
>> get users of the instance
>> users ready
===== gitlab-3 =====
active users (including 1 admins): 9
blocked users: 1
```

## instance.activities

The `instance.activities.js` allows to retrieve the users of a GitLab instance with these informations:

- `id`
- `username`
- `web_url`
- `state`
- `email`
- `is_admin`
- `external`
- `can_create_group`
- `can_create_project`
- `project_access_level` // max role on project
- `group_access_level`   // max role on group
- `last_activity_on`
- `last_activity_at`

### run it

``` 
TOKEN="YOUR_GITLAB_TOKEN" URL="URL_OF_YOUR_GITLAB_INSTANCE" node instance.activities.js NAME_OF_YOUR_GITLAB_INSTANCE
```

- wait ⏳
- then, when finished you can open these reports:
  - `./activities/data_NAME_OF_YOUR_GITLAB_INSTANCE.csv`
  - `./activities/data_NAME_OF_YOUR_GITLAB_INSTANCE.json`

> For example, Here, I run the script 2 times on 2 different GitLab instances

``` 
TOKEN="QDohC2MCsrGfF5rH3a4r" URL="http://gitlab-1.test" node instance.activities.js gitlab-1
TOKEN="zgXDS6o5b2rBBnTHyvu6" URL="http://gitlab-2.test" node instance.activities.js gitlab-2
```

## all.users

This script 
- search all users on several GitLab instances
- check if each user exists several times (on each GitLab) instance

### run it

> First, define the list of the instance in `all.users.config.js` file
> For example:

```javascript
const GLClient = require("../libs/gl-cli").GLClient
module.exports = {
  instances: [
      new GLClient({
        baseUri: `${process.env.URL1}/api/v4`,
        token: process.env.TOKEN1
      })
    , new GLClient({
        baseUri: `${process.env.URL2}/api/v4`,
        token: process.env.TOKEN2
      })
    , new GLClient({
        baseUri: `${process.env.URL3}/api/v4`,
        token: process.env.TOKEN3
      })
  ]
}
```

> And run the script like this example:

```
TOKEN1="QDohC2MCsrGfF5rH3a4r" URL1="http://gitlab-1.test" \
TOKEN2="zgXDS6o5b2rBBnTHyvu6" URL2="http://gitlab-2.test" \
TOKEN3="3wxPZDxQd-GxyrB9_yBo" URL3="http://gitlab-3.test" \
node all.users.js
```

> You'll get a first report like that:

```
==================== REPORT ===================================
 > Total users in all instances     : 30 active and blocked
---------------------------------------------------------------
 > Total users in only one instance : 17
 > Total users in several instances : 3
 > Total remaining users            : 6
---------------------------------------------------------------
 > Control (active)                 : 26
 > Blocked                          : 4
===============================================================
```

> And the script will generate some files in this directory `./all_users`:

- `all-users.csv` and `all-users.json`, this is the concatenation of all the users of all the GitLab instances
- `users-in-only-one-instance,csv` and `users-in-only-one-instance.json`, this is the list of all users existing in only one GitLab instance
- `users-in-several-instances.csv` and `users-in-several-instances.json`, this is the list of the users you can retrieve in more than one GitLab instance


> sample of `users-in-several-instances.json`

```json
[
  {"id":10,"username":"Desmond.Stokes","email":"desmond.stokes@yahoo.com","instance":"gitlab-1.test|gitlab-2.test|gitlab-3.test"},
  {"id":8,"username":"Breanna31","email":"breanna31@yahoo.com","instance":"gitlab-1.test|gitlab-2.test|gitlab-3.test"},
  {"id":1,"username":"root","email":"admin@example.com","instance":"gitlab-1.test|gitlab-2.test|gitlab-3.test"}
]
```

## intersection.users

This script generates 2 files in the `./common_users` directory with the common users between 2 GitLab instances:

- `./common_users/data.csv`
- `./common_users/data.json`

> Run it like that:

```
TOKEN1="QDohC2MCsrGfF5rH3a4r" URL1="http://gitlab-1.test" \
TOKEN2="zgXDS6o5b2rBBnTHyvu6" URL2="http://gitlab-2.test" \
node intersection.users.js
```

> And you'll get a file like that (json version):

```json
[
  {"username":"Desmond.Stokes","email":"desmond.stokes@yahoo.com","instances":[{"id":10,"web_url":"http://gitlab-1.test/Desmond.Stokes","state":"active","is_admin":false,"external":false,"can_create_group":true,"can_create_project":true},{"id":10,"web_url":"http://gitlab-2.test/Desmond.Stokes","state":"active","is_admin":false,"external":false,"can_create_group":true,"can_create_project":true}]},
  {"username":"Kaycee.Dickens47","email":"kaycee.dickens47@hotmail.com","instances":[{"id":9,"web_url":"http://gitlab-1.test/Kaycee.Dickens47","state":"blocked","is_admin":false,"external":false,"can_create_group":true,"can_create_project":true},{"id":9,"web_url":"http://gitlab-2.test/Kaycee.Dickens47","state":"blocked","is_admin":false,"external":false,"can_create_group":true,"can_create_project":true}]},
  {"username":"Breanna31","email":"breanna31@yahoo.com","instances":[{"id":8,"web_url":"http://gitlab-1.test/Breanna31","state":"active","is_admin":false,"external":false,"can_create_group":true,"can_create_project":true},{"id":8,"web_url":"http://gitlab-2.test/Breanna31","state":"active","is_admin":false,"external":false,"can_create_group":true,"can_create_project":true}]},
  {"username":"root","email":"admin@example.com","instances":[{"id":1,"web_url":"http://gitlab-1.test/root","state":"active","is_admin":true,"external":false,"can_create_group":true,"can_create_project":true},{"id":1,"web_url":"http://gitlab-2.test/root","state":"active","is_admin":true,"external":false,"can_create_group":true,"can_create_project":true}]}]
```


## Tools

These scripts could be helpful to prepare some tests: 

> `generate.users.js` allows to create users in a GitLab instance
> Example: generate distinct users in 3 GitLab instances

```
TOKEN="QDohC2MCsrGfF5rH3a4r" URL="http://gitlab-1.test" node generate.users.js
TOKEN="zgXDS6o5b2rBBnTHyvu6" URL="http://gitlab-2.test" node generate.users.js
TOKEN="3wxPZDxQd-GxyrB9_yBo" URL="http://gitlab-3.test" node generate.users.js
```

> `generate.common.users.js` is a sample to create common users in 3 GitLab instances
> Run it like that:

```
TOKEN1="QDohC2MCsrGfF5rH3a4r" URL1="http://gitlab-1.test" \
TOKEN2="zgXDS6o5b2rBBnTHyvu6" URL2="http://gitlab-2.test" \
TOKEN3="3wxPZDxQd-GxyrB9_yBo" URL3="http://gitlab-3.test" \
node generate.common.users.js
```

## Disclaimer :wave:

These scripts are provided for educational purposes. This project is subject to an opensource license (feel free to fork it, and make it better). You can modify the scripts according to your needs. This project is not part of the GitLab support. However, if you need help do not hesitate to create an issue in the associated project, or better to propose a Merge Request.
:warning: Before using these scripts in production, check the consistency of the results on test instances, and of course make backups.


## BTW

:wave: all the tokens are fake tokens 
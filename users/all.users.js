/**
 * Search common users between 2 instances
 * Export the "intersection" to an xlsx file
 * and a Json file
 */

const GLClient = require("../libs/gl-cli").GLClient
const fs = require("fs")
const url = require('url')

const jsonexport = require('jsonexport');

/*
let instances = [
    new GLClient({
      baseUri: `${process.env.URL1}/api/v4`,
      token: process.env.TOKEN1
    })
  , new GLClient({
      baseUri: `${process.env.URL2}/api/v4`,
      token: process.env.TOKEN2
    })
  , new GLClient({
      baseUri: `${process.env.URL3}/api/v4`,
      token: process.env.TOKEN3
    })
]
*/

let instances = require("./all.users.config").instances


async function getAllUsers({glClient, perPage, page}) {
  return glClient   // return a promise
    .get({path: `/users?per_page=${perPage}&page=${page}`})
    .then(response => response.data)
    .catch(error => error)
}


// await is only valid in async function
async function getInstanceUsers(glClient) {
  try {
    var stop = false, page = 1, users = []
    while (!stop) {
      await getAllUsers({glClient, perPage:20, page:page}).then(someUsers => {
        
        users = users.concat(someUsers.map(user => {
          // TODO: get the user role
          return { 
            id: user.id,
            username: user.username, 
            email: user.email,
            instance: url.parse(user.web_url).hostname, // url.parse(glClient.baseUri).hostname
            web_url: user.web_url, 
            state: user.state, 
            is_admin: user.is_admin,
            external: user.external,
            can_create_group: user.can_create_group,
            can_create_project: user.can_create_project,
            processed: false
          }
        }))
        if(someUsers.length == 0) { stop = true }
      })
      page +=1
    }
    return users
  } catch (error) {
    console.log("😡:", error)
  }
}


// url.parse(gitLabClient01.baseUri).hostname
async function batch(instances) {
  let allUsers = []
  for(var i in instances) {
    console.log(instances[i])
    await getInstanceUsers(instances[i]).then(res => {
      console.log(">>", url.parse(instances[i].baseUri).hostname, res.length)
      allUsers = allUsers.concat(res)
      console.log("--->>", allUsers.length)
    })
  }
  console.log("👋 Total", allUsers.length)

  return allUsers

}

batch(instances).then(allUsers => {
  console.log(allUsers.length, "users")
  console.time("Export")

  let usersInOnlyOneInstance = []
  let usersInSeveralInstances = []
  let remaining = []

  allUsers.filter(user=>user.state=="active").forEach(user => {

    //let search = allUsers.filter(item => ((item.email == user.email) && (!user.processed)))  // test username too?
    let search = allUsers.filter(item => item.email == user.email && !user.processed)

    // ⚠️ test status

    console.log(search)

    if(search.length==1) {

      usersInOnlyOneInstance.push({ 
          id: user.id,
          username: user.username, 
          email: user.email,
          instance: user.instance
      })
      user.processed = true
    } 

    if(search.length==0) { // keep the users existing in several instance les one for control

      remaining.push({ 
          id: user.id,
          username: user.username, 
          email: user.email,
          instance: user.instance
      })      
      user.processed = true
    }     
    
    if(search.length>1) {
      search.forEach(element => {
        //allUsers.find(item => element.email == item.email && element.instance == item.instance).processed = true
        element.processed = true
      });

      let record = { 
        id: user.id,
        username: user.username, 
        email: user.email,
        instance: search.map(item => item.instance).join("|")
      }
      // the user exist several time
      usersInSeveralInstances.push(record)

    }

  });

  fs.writeFileSync(`./all_users/remaining.json`, JSON.stringify(remaining))

  jsonexport(remaining, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_users/remaining.csv`, csv)
  })


  fs.writeFileSync(`./all_users/users-in-only-one-instance.json`, JSON.stringify(usersInOnlyOneInstance))

  jsonexport(usersInOnlyOneInstance, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_users/users-in-only-one-instance.csv`, csv)
  })

  fs.writeFileSync(`./all_users/users-in-several-instances.json`, JSON.stringify(usersInSeveralInstances))

  jsonexport(usersInSeveralInstances, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_users/users-in-several-instances.csv`, csv)
  })


  fs.writeFileSync(`./all_users/all-users.json`, JSON.stringify(allUsers))

  jsonexport(allUsers, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_users/all-users.csv`, csv)
  })

  let control = remaining.length+usersInSeveralInstances.length+usersInOnlyOneInstance.length
  let blocked = allUsers.length - control 

  console.log()
  console.log("==================== REPORT ===================================")
  console.log(" > Total users in all instances     :", allUsers.length, "active and blocked")
  console.log("---------------------------------------------------------------")
  console.log(" > Total users in only one instance :", usersInOnlyOneInstance.length)
  console.log(" > Total users in several instances :", usersInSeveralInstances.length)
  console.log(" > Total remaining users            :", remaining.length)
  console.log("---------------------------------------------------------------")
  console.log(" > Control (active)                 :", control)
  console.log(" > Blocked                          :", blocked)
  console.log("===============================================================")
  console.timeEnd("Export")

})


const axios = require("axios");

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.split(search).join(replacement);
};

let tools = {
  urlEncodedPath: ({ name }) => {
    let encoded = `${name
      .replaceAll("/", "%2F")
      .replaceAll(".", "%2E")
      .replaceAll("-", "%2D")
      .replaceAll("_", "%5F")
      .replaceAll(".", "%2E")}`;
    //console.log("👋", encoded);
    return encoded;
  },
  formatText: text => {
    return text
      .split("\n")
      .map(item => item.trim())
      .join("\n");
  }
}

class GLClient {
  constructor({ baseUri, token }) {
    this.baseUri = baseUri;
    this.token = token;
    this.headers = {
      "Content-Type": "application/json",
      "Private-Token": token
    };
  }
  callAPI({ method, path, data }) {
    return axios({
      method: method,
      url: this.baseUri + path,
      headers: this.headers,
      data: data !== null ? JSON.stringify(data) : null
    });
  }
  get({ path }) {
    //console.log("🌍", path);
    return this.callAPI({
      method: "GET",
      path,
      data: null
    });
  }

  delete({ path }) {
    return this.callAPI({
      method: "DELETE",
      path,
      data: null
    });
  }

  post({ path, data }) {
    //console.log("🌍", path);
    return this.callAPI({
      method: "POST",
      path,
      data
    });
  }
  // === features ===
  getAllUsers({ perPage, page }) {
    return this.get({ path: `/users?per_page=${perPage}&page=${page}` })
      .then(response => response.data)
      .catch(error => error);
  }

  getAllProjects({perPage, page}) {
    return this   // return a promise
      .get({path: `/projects?per_page=${perPage}&page=${page}`})
      .then(response => response.data)
      .catch(error => error)
  }

  getAllGroups({perPage, page}) {
    return this   // return a promise
      .get({path: `/groups?per_page=${perPage}&page=${page}`})
      .then(response => response.data)
      .catch(error => error)
  }

  getActivities() {
    return this   // return a promise
      .get({path: `/user/activities`})
      .then(response => response.data)
      .catch(error => error)
  }

  getProjectMembers({projectId}) {
    return this   // return a promise
      .get({path: `/projects/${projectId}/members`})
      .then(response => response.data)
      .catch(error => error)
  }
  
  getGroupMembers({groupId}) {
    return this   // return a promise
      .get({path: `/groups/${groupId}/members`})
      .then(response => response.data)
      .catch(error => error)
  }

  /* ===== Users ===== */
  fetchUserByHandle({ handle }) {
    return this
      .get({
        path: `/users?username=${handle}`
      })
      .then(response => {
        return response.data[0];
      })
      .catch(error => {
        return error;
      });
  }

  fetchUserById({ id }) {
    return this
      .get({
        path: `/users/${id}`
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error;
      });
  }


  fetchUsers({ perPage, page }) {
    return this // return a promise
      .get({ path: `/users?per_page=${perPage}&page=${page}` })
      .then(response => response.data)
      .catch(error => error);
  }

  createUser({username, name, email, password}) {
    return this
      .post({
        path: `/users`,
        data: {
          username: username,
          name: name,
          email: email,
          password: password
        }
      })
      .then(response => response.data)
      .catch(error => error)
  }


  /* ===== Groups ===== */
  /*
  POST /groups
  https://docs.gitlab.com/ee/api/groups.html#new-group
  
  name	string	yes	The name of the group
  path	string	yes	The path of the group
  description	string	no	The group's description
  visibility	string	no	The group's visibility. Can be private, internal, or public.
  
  Add avatar to group: https://gitlab.com/gitlab-org/gitlab-ce/issues/26212
  
  */

  createGroup({ groupName, groupPath, description, visibility }) {
    // return a promise
    return this
      .post({
        path: `/groups`,
        data: {
          name: groupName,
          path: groupPath,
          description: description,
          visibility: visibility
        }
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error;
      });
  }

  getGroupsListByName({ groupName }) {
    return this
      .get({
        path: `/groups?search=${tools.urlEncodedPath({
          name: groupName
        })}`
      })
      .then(response => {
        //console.log("->", response)
        return response.data;
      })
      .catch(error => {
        //console.error(error.response);
        //console.error(error.config);
        return error;
      });
  }

  /* --- add member to a group ---
    POST /groups/:id/members
    https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project

  id	integer/string	yes	The ID or URL-encoded path of the project or group owned by the authenticated user
  user_id	integer	yes	The user ID of the new member
  access_level	integer	yes	A valid access level
  expires_at	string	no	A date string in the format YEAR-MONTH-DAY

  access_level:

  10 => Guest access
  20 => Reporter access
  30 => Developer access
  40 => Maintainer access
  50 => Owner access # Only valid for groups


  TODO: write addMemberByHandleToGroup
  */
  addMemberByIdToGroup({ groupName, userId, accessLevel }) {
    return this
      .post({
        path: `/groups/${tools.urlEncodedPath({ name: groupName })}/members`,
        data: {
          user_id: userId,
          access_level: accessLevel
        }
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error;
      });
  }

  /* --- add member to a project ---
    TODO: 👋
    POST /projects/:id/members
    https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project
  */

  /* ===== Projects ===== */

  createProjectInGroup({ groupName, groupPath, projectName }) {
    //let groupName = "account-management";
    //let groupPath = "gitlab-com/account-management";
    //let projectName = "dwp";

    // 1- get the id of the group
    this
      .get({
        path: `/namespaces?search=${tools.urlEncodedPath({
          name: groupName
        })}`
      })
      .then(response => {
        if (
          response.data[0].name == groupName &&
          response.data[0].full_path == groupPath
        ) {
          let namespaceId = response.data[0].id;
          // 2- create the project
          glClient
            .post({
              path: `/projects`,
              data: {
                name: projectName,
                namespace_id: namespaceId
              }
            })
            .then(response => {
              return response.data;
            })
            .catch(error => {
              return error;
            });
        }
        return response.data;
      })
      .catch(error => {
        return error;
      });
  }

  /* ===== Files ===== */

  createFile({
    projectName,
    nameSpace,
    filePath,
    branch,
    content,
    commitMessage
  }) {
    let id = tools.urlEncodedPath({ name: `${nameSpace}%2F${projectName}` });
    let path = tools.urlEncodedPath({ name: filePath });
    let commit_message = tools.urlEncodedPath({ name: commitMessage });
    let branch_name = tools.urlEncodedPath({ name: branch });

    return this
      .post({
        path: `/projects/${id}/repository/files/${path}?branch=${branch_name}&commit_message=${commit_message}`,
        data: {
          content: tools.formatText(content)
        }
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        console.log(error);
        return error;
      });
  }

  /* ===== CI/CD ===== */

  /* --- Create Group Variables --- 
    https://docs.gitlab.com/ee/api/group_level_variables.html#create-variable

  POST /groups/:id/variables


  id	integer/string	yes	The ID of a group or URL-encoded path of the group owned by the authenticated user
  key	string	yes	The key of a variable; must have no more than 255 characters; only A-Z, a-z, 0-9, and _ are allowed
  value	string	yes	The value of a variable
  protected	boolean	no	Whether the variable is protected

  */
  createGroupVariable({groupName, variable, value, isProtected}) {
    return this
      .post({
        path: `/groups/${tools.urlEncodedPath({ name: groupName })}/variables`,
        data: {
          key: variable,
          value: value,
          protected: isProtected
        }
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error;
      });
  }

    /* ===== Labels ===== */

    createLabel({ name, color, description, userOrGroup, project }) {
      return this
        .post({
          path: `/projects/${tools.urlEncodedPath({
            name: `${userOrGroup}%2F${project}`
          })}/labels`,
          data: { name, color, description }
        })
        .then(response => {
          return response.data;
        })
        .catch(error => {
          return error;
        });
    }

}

module.exports = {
  GLClient: GLClient,
  tools: tools
};
